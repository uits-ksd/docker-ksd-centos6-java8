# docker-eas-centos6-java8
# Built on Base CentOS 6 image with updates and default packages
# Adding Java8 OpenJDK Runtime Environment

FROM uaecs/docker-eas-centos6-base:2018-06-25
MAINTAINER U of A Kuali DevOps <katt-support@list.arizona.edu>

# Install updates
RUN \
  yum update -y && \
  rm -rf /var/lib/apt/lists/* && \
  yum -y install java-1.8.0-openjdk

# University of Arizona CentOS 6 and Java 8 Docker Container

---

This repository is for the Kuali team's Java 8 image based on the Enterprise Application Services CentOS 6 image.

### Description
This project defines an image that will be the base image for the Kuali team's Tomcat 7 + Java 8 Docker image.

### Requirements
This is based on a **docker-eas-centos6-base** tagged image from https://hub.docker.com/r/uaecs/docker-eas-centos6-base/.

### Building
#### Jenkins
The build command we effectively use is: `docker build -t kuali/tomcat7:java8-ua-release-<DATE> --force-rm .`

We then tag and push the image to AWS with the build date (Example: _760232551367.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat7:java8-ua-release-2018-09-19_).

*Caveat:* The Dockerfile of docker-ksd-centos6-java8-tomcat7 will need to be updated with the date of the *java8* image, if a newer one is created.

Jenkins link: https://kuali-jenkins.ua-uits-kuali-nonprod.arizona.edu/jenkins/job/Development/view/Docker%20Baseline/